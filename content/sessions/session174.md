---
title: "جلسه ۱۷۴"
description: "راهکارهای آموزش و فرهنگ ‌سازی استفاده از نرم افزار آزاد"
date: "1398-06-06"
author: "مریم بهزادی"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster174.jpg"
readmore: false
---
[![poster174](../../img/posters/poster174.jpg)](../../img/poster174.jpg)