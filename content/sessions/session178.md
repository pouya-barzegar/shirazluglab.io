---
title: "جلسه ۱۷۸"
description: "معرفی سخت افزار متن باز"
date: "1398-07-17"
author: "مریم بهزادی"
draft: false
categories:
    - "sessions"
summaryImage: "/img/posters/poster178.jpg"
readmore: false
---
[![poster178](../../img/posters/poster178.jpg)](../../img/poster178.jpg)