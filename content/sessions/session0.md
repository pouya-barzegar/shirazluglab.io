---
title: "جلسه ۰"
description: "معرفی شیرازلاگ"
date: "1393-10-17"
categories:
    - "sessions"
readmore: true
---
    چهارشنبه ۱۷ دی ۱۳۹۳ – ۰۹ تا ۱۰

در این جلسه که در سازمان فناوری اطلاعات و ارتباطات (فاوا) شهرداری برگزار شد،
آقای سلمان محمّدی در رابطه با لاگ شیراز و مسایلی که به صورت کلی در لاگ‌ها در
مورد آن‌ها گفت‌وگو می‌شود، صحبت کرد.

**فایل‌ها:**

  * [shirazlug-presentation.odp](http://download.tuxfamily.org/shirazlug/sessions/s0/shirazlug-presentation.odp)
  * [shirazlug-presentation.pdf](http://download.tuxfamily.org/shirazlug/sessions/s0/shirazlug-presentation.pdf)

برای دانلود فایل‌های تمامی جلسه‌ها می‌توانید به [این
پیوند](https://shirazlug.ir/all-sessions-files/ "دانلود فایل‌های تمامی
جلسه‌ها" ) مراجعه نمایید.

**حاضرین:**

  * مهندس يزدان پارساپور (فاوا)
  * مهندس شمس (فاوا)
  * سلمان محمّدی
  * مهندس یزدان‌پناه (فاوا)

